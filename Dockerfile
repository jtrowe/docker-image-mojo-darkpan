ARG BASE_IMG=perl:5.30

FROM ${BASE_IMG}

MAINTAINER Joshua T. Rowe jrowe@jrowe.org

ARG APP_VERSION
RUN cpanm --notest --quiet Mojo::Darkpan@$APP_VERSION

EXPOSE 3000

RUN mkdir --parents /var/lib/mojo-darkpan/darkpan
RUN mkdir --parents /etc/mojo-darkpan
COPY config.json /etc/mojo-darkpan/config.json

ENTRYPOINT [ "darkpan", "--config", "/etc/mojo-darkpan/config.json" ]

