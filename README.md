# Docker-Image-Mojo-Darkpan

Create a Docker image which runs Mojo::Darkpan.

The Mojo::Darkpan repo provides a Dockerfile but as far as I can tell
it's not pushed anywhere.

I am NOT using the Dockerfile provided in the Mojo::Darkpan repo.


# Running The Image

An example using a bind mount for the storage directory.

```sh
docker run --interactive --publish 3000:3000 --rm --tty --volume $PWD/darkpan:/var/lib/mojo-darkpan/darkpan mojo-darkpan:latest
```


## Docker Hub Repo

https://hub.docker.com/r/jtrowe/mojo-darkpan


## License

Copyright (C) jtrowe for the files in this repository.

The actual Mojo::Darkpan code is copyright rshingleton as per 
[Mojo::Darkpan/README.md](https://github.com/rshingleton/Mojo-Darkpan/blob/main/README.md)
.

This library is free software; you can redistribute it and/or modify it
under the same terms as Perl itself.

## See Also

*   https://metacpan.org/pod/Mojo::Darkpan
*   https://github.com/rshingleton/Mojo-Darkpan

